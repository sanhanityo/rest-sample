<?php
use Restserver\Libraries\REST_Controller;

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class User extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index_get()
	{
		// $data_siswa = array(
		// 	"error_message" => "something wrong"
		// );

		$this->response('err', 500);	
	}
	public function index_post()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
	
		if ($this->form_validation->run() == FALSE)
		{
			$this->response('sad', 200);
		}
		else
		{
			$this->response([
				'status' 	=> 'ok',
				'data'		=> [
					'username'	=> $username,
					'password'	=> $password
				]
			], 200);
		}
	}
}
